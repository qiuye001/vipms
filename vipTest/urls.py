from django.conf.urls import patterns, include, url
import  vip.views as views
from django.contrib import admin
from vipTest import settings
from django.conf.urls.static import static
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'vipTest.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^admin/', include(admin.site.urls)),
    # Your site patterns here:
    url(r'^$', views.login, name='login'),
    url(r'^login/', views.login, name="login"),
    url(r'^accounts/login/', views.login, name="login"),
    url(r'^login.html', views.login, name="login"),
    url(r'^logout/', views.logout, name="logout"),
    url(r'^index/', views.home,name="home"),
    url(r'^home/', views.home,name="home"),

    url(r'^vip_buy/', views.vip_buy,name="vip_buy"),
    url(r'^vip_buy.html/', views.vip_buy,name="vip_buy"),
    url(r'^vip_search_by_number/', views.vip_search_by_number,name="vip_search_by_number"),
    url(r'^vip_search_by_id/', views.vip_search_by_id,name="vip_search_by_id"),
    url(r'^buy_add/', views.buy_add,name="buy_add"),

    url(r'^vip_manager/', views.vip_manager,name="vip_manager"),
    url(r'^vip_add/', views.vip_add,name="vip_add"),
    url(r'^check_card_number/', views.check_card_number,name="check_card_number"),
    url(r'^check_mobile_phone/', views.check_mobile_phone,name="check_mobile_phone"),
    url(r'^update_grade/', views.update_grade,name="update_grade"),

    url(r'^vip_exchange/', views.vip_exchange,name="vip_exchange"),
    url(r'^exchange_add/', views.exchange_add,name="exchange_add"),
    url(r'^update_vip/', views.update_vip,name="update_vip"),
    url(r'^delete_vip/', views.delete_vip,name="delete_vip"),

    url(r'^vip_bill/', views.vip_bill,name="vip_bill"),
    url(r'^update_bill/', views.update_bill,name="update_bill"),
    url(r'^delete_bill/', views.delete_bill,name="delete_bill"),
    url(r'^update_bill_sale/', views.update_bill_sale,name="update_bill_sale"),
    url(r'^delete_bill_sale/', views.delete_bill_sale,name="delete_bill_sale"),

     # static urlpatterns
    # Static files (CSS, JavaScript, Images)
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root':settings.STATIC_ROOT}),
)
handler404 = views.NF404_viewer
handler500 = views.NF500_viewer
