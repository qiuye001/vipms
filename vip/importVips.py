# coding=utf-8
__author__ = 'XIAO'
'''
从excel表中导入新会员信息到数据库，改操作为追加操作，如果信息有重复，请清除原有数据表内容后再操作。
'''
import xlrd
import MySQLdb
# 连接
conn = MySQLdb.connect(host="127.0.0.1", user="root", passwd="123456", db="vipms", charset="utf8")
cursor = conn.cursor()

#写入
data = xlrd.open_workbook("d://Book11.xls")
table = data.sheet_by_index(0)
rows = table.nrows

print "共有", rows, "个新会员"
v = 0
try:
    for i in range(rows):
        v = i
        card_number = str((table.cell_value(i, 0))).replace(".0", "")
        mobile_phone = str((table.cell_value(i, 1))).replace(".0", "")
        name = table.cell_value(i, 2)

        # 如果会员号为空，则把手机号作为会员号
        if not card_number.strip():
            card_number = mobile_phone
            print "会员卡号为空，手机号码作为会员号", card_number, mobile_phone, name

        # 如果会员姓名为空，则采用默认名字
        if not name.strip():
            name = "神秘会员"

        sql = "insert into vip_vip(card_number, mobile_phone, name, auth_id) values(%s, %s, %s, %s)"
        param = [card_number, mobile_phone, name, 1]
        cursor.execute(sql, param)

    #提交生效
    conn.commit()
except:
    print "导入数据出错。。。"
    print table.row_values(v)
#关闭
cursor.close()
conn.close()