#coding=utf-8
from django.db import models

# Create your models here.

'''会员基本信息'''
class Vip(models.Model):
    id = models.AutoField(primary_key=True)
    card_number = models.CharField(max_length=30, db_index=True)
    mobile_phone = models.CharField(max_length=11, db_index=True)
    name = models.CharField(max_length=30, db_index=True)
    gender = models.CharField(max_length=1, default="0")
    address = models.CharField(max_length=50)
    points = models.IntegerField(max_length=11, default=0)
    total_points = models.IntegerField(max_length=11, default=0)
    last_visit = models.DateTimeField()
    is_state = models.IntegerField(max_length=1, default=1)
    auth_id = models.IntegerField(max_length=11)

    def __unicode__(self):
        return u'%s, %s, %s, %s, %s, %s' % (self.card_number, self.name, self.mobile_phone, self.points, self.total_points, self.last_visit)


'''会员购买记录'''
class Buy(models.Model):
    id = models.AutoField(primary_key=True)
    vip_id = models.IntegerField(max_length=11, db_index=True)
    visit_time = models.DateTimeField()
    amount =models.DecimalField(max_digits=11, decimal_places=2)
    description = models.CharField(max_length=1250)
    auth_id = models.IntegerField(max_length=11)

    def __unicode__(self):
        return u'%s, %s, %s, %s' % (self.vip_id, self.visit_time, self.amount, self.description)


'''积分兑换记录'''
class Exchange (models.Model):
    id = models.AutoField(primary_key=True)
    vip_id = models.IntegerField(max_length=11, db_index=True)
    visit_time = models.DateTimeField()
    points = models.IntegerField(max_length=11)
    description = models.CharField(max_length=1250)
    auth_id = models.IntegerField(max_length=11)

    def __unicode__(self):
        return u'%s, %s, %s, %s' % (self.vip_id, self.visit_time, self.points, self.description)

'''进货记录'''
class Bill(models.Model):
    id = models.AutoField(primary_key=True)
    visit_time = models.DateField()
    amount =models.DecimalField(max_digits=11, decimal_places=2)
    description = models.CharField(max_length=1250)
    auth_id = models.IntegerField(max_length=11)
    type = models.IntegerField(max_length=1)
    def __unicode__(self):
        return u'%s, %s, %s, %s' % (self.id, self.visit_time, self.amount, self.description)

'''会员积分与折扣信息'''
class Grade(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=45)
    points_begin = models.IntegerField(max_length=11)
    points_end = models.IntegerField(max_length=11)
    discount = models.DecimalField(max_digits=5, decimal_places=2)
    def __unicode__(self):
        return u'%s, %s, %s, %s' % (self.discount, self.name, self.points_begin, self.points_end)