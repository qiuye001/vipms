# coding=utf-8

from django.http import HttpResponse
from django.template.response import TemplateResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login as user_login, logout as user_logout
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.shortcuts import render_to_response, RequestContext
from vip.models import Vip, Buy, Exchange, Bill, Grade
import json
from datetime import datetime
from django.db.models import Q
from vipTest.settings import POINTS
from vip.forms import AddBill
from django.core.paginator import Paginator
from django.core.paginator import PageNotAnInteger
from django.core.paginator import EmptyPage
from django.db import connection
import random
from vipTest.settings import PAGE_SIZE
# Create your views here.

@csrf_exempt
def login(request):
    '''登陆后的默认跳转地址'''
    redirect_to = "/home"
    logo = random.randint(0,10)
    if request.user.is_authenticated():
        return HttpResponseRedirect(redirect_to)
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                user_login(request, user)
                return HttpResponseRedirect(redirect_to)
            else:
                return render_to_response("login.html", {"message": "用户没有启用！","logo":logo},
                                          context_instance=RequestContext(request))
        else:
            return render_to_response("login.html", {"message": "账号或密码错误！","logo":logo}, context_instance=RequestContext(request))
    else:
        return render_to_response('login.html',{"logo":logo})


def logout(request):
    user_logout(request)
    return HttpResponseRedirect('/login.html')

@login_required
def home(request):
    return render(request, "home.html")


'''添加会员'''
@csrf_exempt
@login_required
def vip_add(request):
    card_number = request_GET(request, "card_number")
    mobile_phone = request_GET(request, "mobile_phone")
    name = request_GET(request, "name")
    print  card_number,mobile_phone, name
    if name.strip()=='':
        name="神秘会员"
    address = request_GET(request, "address")
    gender = request_GET(request, "gender")
    user = request.user
    print  card_number,mobile_phone, name
    '''检测手机号和会员号的可用性'''
    if check_exist_card_number(card_number):
        context = {
        "vip": None,
        "info": "添加会员操作失败！"
        }

        return render(request, "error.html", context)
    else:
        vip = Vip.objects.create(card_number=card_number, mobile_phone=mobile_phone, name=name,
                                 points=0, total_points=0, gender=gender, address=address, auth_id=user.id)
        vip.save()
    context = {
        "vip": vip,
        "info": "添加会员操作成功！"
    }

    return render(request, "success.html", context)


'''添加购买记录'''
@csrf_exempt
@login_required
def buy_add(request):
    vip_id = request.GET.get("vip_id")
    amount = request.GET.get("amount")
    amount = float(amount)
    description = request.GET.get("description")
    if (amount) <= 0:
        return render(request, "error.html", ({"info": "输入金额数据错误，操作失败！"}))

    # 保存购买记录
    user = request.user
    visit_time = datetime.today()
    buy = Buy.objects.create(vip_id=vip_id, visit_time=visit_time, amount=amount, description=description,auth_id=user.id)
    buy.save()
    #更新积分信息
    new_points = (int)(float(amount)) * POINTS
    vip = Vip.objects.get(id=vip_id)
    vip.points = vip.points + new_points
    vip.last_visit = visit_time
    vip.total_points = vip.total_points + new_points
    vip.save()
    context = {
        "vip": vip,
        "info": "添加购买记录操作成功！"
    }

    return render(request, "success.html", context)


'''添加积分兑换记录'''
@csrf_exempt
@login_required
def exchange_add(request):

    vip_id = request.GET.get("vip_id")
    points = request.GET.get("points")
    description = request.GET.get("description")
    vip = Vip.objects.get(id=vip_id)
    if (int)(points) <= 0 or (int)(points) > vip.points:
        return render(request, "error.html", ({"info": "输入积分数据错误，操作失败！"}))

    # 保存兑换记录
    user = request.user
    visit_time = datetime.today()
    exchange = Exchange.objects.create(vip_id=vip_id, visit_time=visit_time, points=points, description=description,auth_id=user.id)
    exchange.save()
    #更新积分信息

    vip.points = vip.points - ((int)(points))
    vip.last_visit = visit_time
    vip.save()
    context = {
        "vip": vip,
        "info": "成功兑换礼品！" + "消耗积分：" + str(points)
    }
    return render(request, "success.html", context)


'''检测会员卡卡号是否可用，返回json'''
@csrf_exempt
@login_required
def check_card_number(request):
    card_number = request.GET.get("card_number")
    response_data = {"code": 0, "info": "该号码已被使用"}
    if check_exist_card_number(card_number) is False:
        response_data["code"] = 1
        response_data["info"] = "该号码可以使用"
    return HttpResponse(json.dumps(response_data), content_type="application/json")


'''检测手机号码是否可用，返回json'''
@csrf_exempt
@login_required
def check_mobile_phone(request):
    mobile_phone = request.GET.get("mobile_phone")
    response_data = {"code": 0, "info": "该号码已被使用"}
    if check_exist_mobile_phone(mobile_phone) is False:
        response_data["code"] = 1
        response_data["info"] = "该号码可以使用"
    return HttpResponse(json.dumps(response_data), content_type="application/json")


'''会员资料查询'''

@csrf_exempt
@login_required
def vip_search_by_id(request):
    search = request.GET.get("vip_id")
    vip = Vip.objects.get(id=search)
    dic = {}
    dic["id"] = vip.id
    dic["points"] = vip.points
    dic["card_number"] = vip.card_number
    dic["mobile_phone"] = vip.mobile_phone
    dic["name"] = vip.name
    if vip.last_visit:
        dic["last_visit"] = vip.last_visit.strftime('%Y-%m-%d');
    else:
        dic["last_visit"] = "0000-00-00";
    grade = Grade.objects.all().order_by("-discount")
    for g in grade:
        if vip.points >= g.points_begin and vip.points <=g.points_end:
            dic["total_points"] = g.name
            dic["discount"] = str(g.discount)
            break

    return HttpResponse(json.dumps(dic), content_type="application/json")


'''根据输入的会员卡卡号信息，自动匹配卡号'''
@csrf_exempt
@login_required
def vip_search_card_number(request):
    search = request.GET.get("card_number")
    vips = Vip.objects.filter(card_number__icontains=search)[0:9]
    dic ={}
    for vip in vips:
        dic[vip.id] = vip.card_number
    return HttpResponse(json.dumps(dic), content_type="application/json")


'''根据输入的手机号信息，自动匹配手机号'''
@csrf_exempt
@login_required
def vip_search_mobile_phone(request):
    search = request.GET.get("mobile_phone")
    vips = Vip.objects.filter(mobile_phone__icontains=search)[0:9]
    dic ={}
    for vip in vips:
        dic["data"] = vip.ip
        dic["value"] = vip.mobile_phone
    return HttpResponse(json.dumps(dic), content_type="application/json")


'''根据输入的数字自动匹配手机号和卡号'''
'''
Ajax AutoComplete for jQuery
Response 数据结构
后台返回数据必须为JSON格式的JS对象.

{
    query: "Unit",
    suggestions: [
        { value: "United Arab Emirates", data: "AE" },
        { value: "United Kingdom",       data: "UK" },
        { value: "United States",        data: "US" }
    ]
}
'''
@csrf_exempt
@login_required
def vip_search_by_number(request):
    search = request.GET.get("query")
    sql_search = "(SELECT a.id as id, a.card_number as value FROM vip_vip a where a.card_number like '%%" + search + "%%' limit 0,10) " \
                 "union (SELECT b.id as id, b.mobile_phone as value FROM vip_vip b where b.mobile_phone like '%%" + search + "%%' limit 0,10) " \
                 "union (SELECT c.id as id , c.name as value FROM vip_vip c where c.name like '%%" + search + "%%')"
    print sql_search
    cursor = connection.cursor()
    cursor.execute(sql_search)
    vips = cursor.fetchall()
    dics = []
    for vip in vips:
        dic = {}
        dic["data"] = "m" + str(vip[0])
        dic["value"] = vip[1]
        dics.append(dic)
    '''
    vips = Vip.objects.filter(Q(name__icontains=search)|Q(mobile_phone__icontains=search)|Q(card_number__icontains=search))[0:9]
    dics = []
    for vip in vips:
        dic1 = {}
        dic1["data"] = "m" + str(vip.id)
        dic1["value"] = vip.mobile_phone
        dic2 = {}
        dic2["data"] = "c" + str(vip.id)
        dic2["value"] = vip.card_number
        dics.append(dic1)
        dics.append(dic2)
        '''
    print search
    print dics
    response_data = {}
    response_data["query"] = "Unit"
    response_data["suggestions"] = dics
    return HttpResponse(json.dumps(response_data), content_type="application/json")

'''检测手机号是否存在'''
def check_exist_mobile_phone(mobile_phone):
    vips = Vip.objects.filter(mobile_phone=mobile_phone)
    print "helloollllll"
    if vips.__len__() is 0:
        return False
    else:
        return True

'''检测卡号是否存在'''
def check_exist_card_number(card_number):
    vips = Vip.objects.filter(card_number=card_number)
    if vips.__len__() is 0:
        return False
    else:
        return True

'''根据字段名获取 get 的值'''
def request_GET(request, paramStr):
    try:
        paramStr = request.GET.get(paramStr)
    except:
        paramStr = None
    return paramStr

'''跳转到会员购物页面'''
@csrf_exempt
@login_required
def vip_buy(request):
    return render(request, "vip_buy.html")

'''跳转到会员管理页面'''
@csrf_exempt
@login_required
def vip_manager(request):
    search = request.GET.get('search')
    if search is None:
        vips_list=Vip.objects.all().order_by("-last_visit")
    else:
        vips_list = Vip.objects.filter(Q(name__icontains=search)|Q(mobile_phone__icontains=search)|Q(card_number__icontains=search))
    content={}
    pn = request.GET.get('pn')          # 分页功能
    if pn is None:
        pn=1;
    pn = int(pn)

    paginator  = Paginator(vips_list,PAGE_SIZE)
    try:
        vips = paginator.page(pn)
    except PageNotAnInteger:
        vips = paginator.page(1)
    except EmptyPage:
        vips = paginator.page(paginator.num_pages)
    vip_count=Vip.objects.all().count()
    content["vips"]=vips
    content["vip_count"]=vip_count
    content["search"]=search
    #获取会员等级设置信息
    grade = Grade.objects.all().order_by("-discount")
    content["grade"] = grade
    return TemplateResponse(request, "vip_manager.html", content)

'''跳转到积分兑换页面'''
@csrf_exempt
@login_required
def vip_exchange(request):
    return render(request, "vip_exchange.html")

'''跳转到进销管理页面'''
@csrf_exempt
@login_required
def vip_bill(request):
    active = request.GET.get('active')
    if active is None:
        active = "default"
    content={}
    if request.method == "POST":
        form = AddBill(request.POST)
        if form.is_valid():
            user = request.user
            Bill.objects.create(
                visit_time=form.cleaned_data['visit_time'],
                amount=form.cleaned_data['amount'],
                auth_id = user.id
            )
       # return TemplateResponse(request, "vip_bill.html", content)
    else:
        form = AddBill()

    pn = request.GET.get('pn')          # 分页功能
    if pn is None:
        pn=1;
    pn = int(pn)
    bill_list=Bill.objects.filter().order_by("-visit_time","-id")     #查询TestCase表中的登陆用户的数据，按照时间逆序排序
    paginator  = Paginator(bill_list, PAGE_SIZE)
    try:
        bill = paginator.page(pn)
    except PageNotAnInteger:
        bill = paginator.page(1)
    except EmptyPage:
        bill = paginator.page(paginator.num_pages)

    pn2 = request.GET.get('pn2')          # 分页功能
    if pn2 is None:
        pn2=1;
    pn2 = int(pn2)
    sql = "SELECT vip_buy.id as id, vip_vip.name as name, vip_vip.card_number as card_number, "\
            "vip_buy.amount as amount, vip_buy.visit_time as visit_time , vip_buy.description as description FROM vip_vip, vip_buy " \
            "where vip_vip.id = vip_buy.vip_id order by vip_buy.visit_time desc, vip_buy.id desc"
    cursor = connection.cursor()
    cursor.execute(sql)
    sale_info= cursor.fetchall()
    print "sale_info", (sale_info)
    paginator2  = Paginator(sale_info, PAGE_SIZE)
    try:
        sale = paginator2.page(pn2)
    except PageNotAnInteger:
        sale = paginator2.page(1)
    except EmptyPage:
        sale = paginator2.page(paginator2.num_pages)

    pn3 = request.GET.get('pn3')          # 分页功能
    if pn3 is None:
        pn3=1;
    pn3 = int(pn3)
    cursor = connection.cursor()
    cursor.execute("SELECT p.MONTH as month,a.buy,p.sale FROM (SELECT DATE_FORMAT(vip_bill.visit_time,'%Y-%m') AS MONTH,SUM(vip_bill.amount) AS buy FROM vip_bill  GROUP BY MONTH) AS a RIGHT JOIN (SELECT DATE_FORMAT(visit_time,'%Y-%m') AS MONTH,SUM(vip_buy.amount) AS sale FROM vip_buy GROUP BY MONTH) AS p ON a.MONTH=p.MONTH  GROUP BY p.MONTH ")
    all_bill_list = cursor.fetchall()
    for b in all_bill_list:
       print b[0]
    paginator3  = Paginator(all_bill_list, PAGE_SIZE)
    try:
        all_bill = paginator3.page(pn3)
    except PageNotAnInteger:
        all_bill = paginator3.page(1)
    except EmptyPage:
        all_bill = paginator3.page(paginator3.num_pages)

    content["all_bill"]=all_bill
    content["bill"]=bill
    content["sale"]=sale
    content["active"]=active
    print "active", active
    return TemplateResponse(request, "vip_bill.html", content)

'''更新进货记录'''
@csrf_exempt
@login_required
def update_bill(request):
    response_data={}
    try:
        id = request.POST.get("id")
        form = AddBill(request.POST)
        bill=Bill.objects.get(id=id)
        bill.amount=request.POST.get('amount')
        bill.visit_time=request.POST.get('visit_time')
        print bill
        bill.save()
        response_data["sucess"]=True
    except:
        response_data["sucess"]=False
    return HttpResponse(json.dumps(response_data), content_type="application/json")

'''删除进货记录'''
@csrf_exempt
@login_required
def delete_bill(request):
    response_data={}
    try:
        id = request.POST.get("id")
        bill=Bill.objects.get(id=id)
        bill.delete()
        response_data["sucess"]=True
    except:
        response_data["sucess"]=False
    return HttpResponse(json.dumps(response_data), content_type="application/json")

'''更新销售记录'''
@csrf_exempt
@login_required
def update_bill_sale(request):
    response_data={}
    print "========="
    try:
        id = request.POST.get("id")
        buy=Buy.objects.get(id=id)
        buy.amount=request.POST.get('amount')
        buy.visit_time=request.POST.get('visit_time')
        print buy
        buy.save()
        response_data["sucess"]=True
    except:
        response_data["sucess"]=False
    return HttpResponse(json.dumps(response_data), content_type="application/json")

'''删除销售记录'''
@csrf_exempt
@login_required
def delete_bill_sale(request):
    print "delete_bill_dale"
    response_data={}
    try:
        id = request.POST.get("id")
        print "delete_bill_dale",id
        buy=Buy.objects.get(id=id)
        now_time = datetime.today()
        print "now_time", now_time , "visit_time",buy.visit_time
        dd = (now_time-buy.visit_time).seconds/(3600) + (now_time-buy.visit_time).days*24
        print "距离购买时间-间隔", dd , "小时"
        if (dd) > 24:
            print "超出可修改时间"
            response_data["sucess"]=False
            response_data["info"]="订单超出可修改时间范围！"
        else:

            delete_points = (int)(buy.amount) * POINTS
            vip = Vip.objects.get(id=buy.vip_id)
            vip.points = vip.points - delete_points
            vip.total_points = vip.total_points - delete_points
            vip.save()
            buy.delete()
            response_data["sucess"]=True
    except:
        response_data["sucess"]=False
        response_data["info"] = "操作异常，请稍后再试！"
    return HttpResponse(json.dumps(response_data), content_type="application/json")

'''更新会员信息'''
@csrf_exempt
@login_required
def update_vip(request):
    response_data={}
    try:
        id = request.POST.get("id")
        vip=Vip.objects.get(id=id)
        vip.name=request.POST.get('name')
        vip.mobile_phone=request.POST.get('mobile_phone')
        vip.card_number=request.POST.get('card_number')
        vip.save()
        response_data["sucess"]=True
    except:
        response_data["sucess"]=False
    return HttpResponse(json.dumps(response_data), content_type="application/json")

'''删除会员记录'''
@csrf_exempt
@login_required
def delete_vip(request):
    response_data={}
    try:
        id = request.POST.get("id")
        vip=Vip.objects.get(id=id)
        vip.delete()
        print "delete success"
        response_data["sucess"]=True
    except:
        print "delete false"
        response_data["sucess"]=False
    return HttpResponse(json.dumps(response_data), content_type="application/json")

'''更新会员折扣信息'''
def update_grade(request):
    #transaction.commit()
    try:
        grades = Grade.objects.all()
        pre_grades = []
        for grade in grades:
            pre_grades.append(grade)
        for i in range(1,6):
            name = request_GET(request,"grade_name_"+str(i))
            points_begin = request_GET(request,"grade_points_begin_"+str(i))
            points_end = request_GET(request,"grade_points_end_"+str(i))
            discount = request_GET(request,"grade_discount_"+str(i))
            print name, points_begin , points_end, discount
            grade  = Grade.objects.create(
                name = name,
                points_begin = points_begin,
                points_end = points_end,
                discount = discount,
            )
            print grade
            grade.save()
        context = {
                "vip": None,
                "info": "修改会员折扣信息操作成功！"
        }
        #transaction.commit()
        for grade in pre_grades:
            grade.delete()
        return render(request, "success.html", context)
    except:
        #transaction.rollback()
        Grade.objects.all().delete()
        for grade in pre_grades:
            grade.save()
        context = {
        "vip": None,
       "info": "修改会员折扣信息操作失败！"
        }
        return render(request, "error.html", context)

def NF404_viewer(request):
    return render(request, "404.html")

def NF500_viewer(request):
    return render(request, "500.html")