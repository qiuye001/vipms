﻿/*jslint  browser: true, white: true, plusplus: true */
/*global $, countries */

$(function () {
    'use strict';
/*
Response 数据结构
后台返回数据必须为JSON格式的JS对象.

{
    query: "Unit",
    suggestions: [
        { value: "United Arab Emirates", data: "AE" },
        { value: "United Kingdom",       data: "UK" },
        { value: "United States",        data: "US" }
    ]
}
 */
    // Initialize ajax autocomplete:
    //输入会员号或者手机号，自动完成下拉候选菜单，并显示会员详细信息
    $('#autocomplete-ajax').autocomplete({
        serviceUrl: '/vip_search_by_number/',
        deferRequestBy:500,
        onSelect: function(suggestion) {
            $("#vip_id").val(suggestion.data.substr(1));
            $.get("/vip_search_by_id/",{vip_id:suggestion.data.substr(1)},function(data){
               show_vip_info(data);
            });
        },
        onInvalidateSelection: function() {
            $("#vip_id").val("");
            vip_info_reset();
            $("#preamount").val("");
        }
    });

    //键入金额时候，自动计算打折后的进而, num.toFixed(2),自动修正小数点后两位
    $("#preamount").keyup(function(){
        if($("#vip_id").val()=="" || $("#vip_id").val() ==0){
            $('#autocomplete-ajax').focus();
            $("#preamount").val("");
            return;
        }

        $("#form-group-amount").removeClass("has-error");
        var amount = $(this).val();
        var discount = $("#info_discount").html();
        if($.isNumeric(amount)&&amount>0){
              $("#discount").html("￥"+(discount*amount).toFixed(2));
             $("#amount").val((discount*amount).toFixed(2));
        }
        else{
            $("#discount").html("￥00.00");
            $("#amount").val("");
            $("#form-group-amount").addClass("has-error");
            if(amount==0 || amount==""){
            }
            else{
                alert("金额输入错误！");
            }
        }
    });

    //添加会员时，输入新会员卡号信息，失去焦点时，自动验证可用情况
    $("#vip_add_manager_card_number").blur(function(){
        var card_number = $("#vip_add_manager_card_number").val();
        if(card_number==""){
            $("#for_group_vip_add_manager_card_number").addClass("has-error")
            $("#check_info_vip_add_manager_card_number").html("");
            return;
        }
        $.get("/check_card_number/", {card_number:card_number}, function(data){
            if(data.code==1){
                $("#for_group_vip_add_manager_card_number").removeClass("has-error")
                $("#for_group_vip_add_manager_card_number").addClass("has-success")
            }else{
                $("#for_group_vip_add_manager_card_number").removeClass("has-success")
                $("#for_group_vip_add_manager_card_number").addClass("has-error")
            }
            $("#check_info_vip_add_manager_card_number").html(data.info);
        });
    });
    //添加会员时，输入新手机号码信息，失去焦点时，自动验证可用情况
    $("#vip_add_manager_mobile_phone").blur(function(){
        var mobile_phone = $("#vip_add_manager_mobile_phone").val();
        if(mobile_phone==""){
            $("#for_group_vip_add_manager_mobile_phone").removeClass("has-error")
            $("#for_group_vip_add_manager_mobile_phone").removeClass("has-success")
            $("#check_info_vip_add_manager_mobile_phone").html("");
            return;
        }
        if(!checkMobile(mobile_phone)){
            $("#for_group_vip_add_manager_mobile_phone").addClass("has-error")
            $("#check_info_vip_add_manager_mobile_phone").html("");
            return;
        }
        $.get("/check_mobile_phone/", {mobile_phone:mobile_phone}, function(data){
            if(data.code==1){
                $("#for_group_vip_add_manager_mobile_phone").removeClass("has-error")
                $("#for_group_vip_add_manager_mobile_phone").addClass("has-success")
            }else{
                $("#for_group_vip_add_manager_mobile_phone").removeClass("has-success")
                $("#for_group_vip_add_manager_mobile_phone").addClass("has-error")
            }
            $("#check_info_vip_add_manager_mobile_phone").html(data.info);
        });
    });

  $('input').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
    increaseArea: '20%' // optional
  });

    $("input,textarea").focus(function(){
      var default_lable=$(this).attr("placeholder");
      $(this).attr("default_lable",default_lable);
      $(this).attr("placeholder","");
    });
     $("input,textarea").blur(function(){
         if($(this).val()==""){
             $(this).attr("placeholder",$(this).attr("default_lable"));
         }
    });
    //自动识别url中的卡号参数信息
    if(getQueryString("card_number")){
        $('#autocomplete-ajax').val(getQueryString("card_number"));
        $('#autocomplete-ajax').focus();
    }


}); //end for_document ready $()

//提交前检查会员购物表单
function check_vip_buy_form(){
    var amount = $("#amount").val();
    var vip_id = $("#vip_id").val();
    if(vip_id=="" ||$("#autocomplete-ajax").val()=="" ){
        $("#autocomplete-ajax").focus();
        return false;
    }
    if(amount==0 ||amount==""){
        $("#preamount").focus();
        return false;
    }
    if(!confirm("订单金额："+amount+"，确认提交？")){
        return false;
    }
}

//新增会员信息
function check_vip_add_form(){

        $.get("/vip_add/",$('#vip_manager_add_form').serialize(),function(data){
            if(data.code==0){
                alert("添加会员操作失败，请重试！")
                return false;
            }
            else if(data.code==1){
                alert("操作成功！")
               $.get("/vip_search_by_id/",{vip_id:data.vip_id},function(vip){
                    show_vip_info(vip);
                   $("#vip_add_reset_button").click();
                   return false;
                });
            }
        });
        return false;
}

//清除页面上显示的会员信息
function vip_info_reset(){
    $(".vip-info").html("...");
    $(".form-group").removeClass("has-error");
    $(".form-group").removeClass("has-success");
}

//添加会员表单重置
function vip_add_reset(){
    $(".control-label-info").html("");
    $(".form-group").removeClass("has-error");
    $(".form-group").removeClass("has-success");

}

//检测手机号码
function checkMobile(str) {
    var  re = /^1\d{10}$/
    if (re.test(str)) {
        return true
    } else {
        alert("不是一个合法的手机号码！")
        return false;
    }
}
//侧边栏展示会员信息
function show_vip_info(data){
     $("#info_name").html(data.name);
     $("#info_card_number").html(data.card_number);
     $("#info_mobile_phone").html(data.mobile_phone);
     $("#info_points").html(data.points);

     $("#goto_vip_exchange").attr("href",$("#goto_vip_exchange").attr("href_value")+"?card_number="+data.card_number)
     $("#info_total_points").html(data.total_points);
     $("#info_discount").html(data.discount);
     $("#info_last_visit").html(data.last_visit);

}
 //x修改进货记录
function Edit(time,amount,id){
     $("#"+id+"_visit_time").html("<input class='Wdate update-input ' id='"+id+"_visit_time_input' name='visit_time' value='"+time+"' onClick='WdatePicker()' />");
     $("#"+id+"_amount").html("<input class='form-control update-input' id='"+id+"_amount_input' value='"+amount+"' name='amount' />");
     $("#"+id+"_save").html("<a onclick='return Save("+id+");'>保存</a>");
}
//提交修改后的进货记录
function Save(id){
     $.post("/update_bill/",{id:id,visit_time:$("#"+id+"_visit_time_input").val(),amount:$("#"+id+"_amount_input").val()},function(data){
         if(data.sucess){
            $("#"+id+"_visit_time").html($("#"+id+"_visit_time_input").val());
            $("#"+id+"_amount").html($("#"+id+"_amount_input").val());
            $("#"+id+"_save").html("<a onclick='return Delete("+id+");'>删除</a>");
            return false;
         }
         else
           alert("添加失败！");
           return false
        });
}

//删除进货记录
function Delete(id){
    if(confirm("确认删除该记录？")){
             $.post("/delete_bill/",{id:id},function(data){
             if(data.sucess){
                 $("#"+id+"_tr").remove();
                 return false;
             }
             else
               alert("操作失败！");
               return false
            });
    }else{
        return false;
    }
}

//获取url参数
function getQueryString(name) {
      var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
      var r = window.location.search.substr(1).match(reg);
      if (r != null) return unescape(r[2]); return null;
}

//修改销售记录
function Edit_sale(time,amount,id){
     $("#"+id+"_visit_time_sale").html("<input class='Wdate update-input ' id='"+id+"_visit_time_input_sale' name='visit_time' value='"+time+"' onClick='WdatePicker()' />");
     $("#"+id+"_amount_sale").html("<input class='form-control update-input' id='"+id+"_amount_input_sale' value='"+amount+"' name='amount' />");
     $("#"+id+"_save_sale").html("<a onclick='return Save_sale("+id+");'>保存</a>");
}
//提交修改销售记录
function Save_sale(id){
     $.post("/update_bill_sale/",{id:id,visit_time:$("#"+id+"_visit_time_input_sale").val(),amount:$("#"+id+"_amount_input_sale").val()},function(data){
         if(data.sucess){
            $("#"+id+"_visit_time_sale").html($("#"+id+"_visit_time_input_sale").val());
            $("#"+id+"_amount_sale").html($("#"+id+"_amount_input_sale").val());
            $("#"+id+"_save_sale").html("<a onclick='return Delete_sale("+id+");'>删除</a>");
            return false;
         }
         else
           alert("添加失败！");
           return false
        });
}

//删除销售记录
function Delete_sale(id){
    if(confirm("确认删除该记录？")){
             $.post("/delete_bill_sale/",{id:id},function(data){
             if(data.sucess){
                 $("#"+id+"_tr_sale").remove();
             }
             else {
                 alert("操作失败！"+data.info);
             }
             return false
            });
    }else{
        return false;
    }
}

//修改会员信t
function Edit_vip(id, card_number, name, mobile_phone){
    $("#"+id+"_name_vip").html("<input class='update-input_text form-control' id='"+id+"_name_vip_input' name='name' value='"+name+"'/>");
    $("#"+id+"_mobile_phone_vip").html("<input class='update-input_text form-control' id='"+id+"_mobile_phone_vip_input' name='mobile_phone' value='"+mobile_phone+"'/>");
    $("#"+id+"_card_number_vip").html("<input class='update-input_text form-control' id='"+id+"_card_number_vip_input' name='card_number' value='"+card_number+"'/>");
    $("#"+id+"_save_vip").html("<a onclick='return Save_vip("+id+");'>保存</a>");
}

//t提交会员信息的修改数据
function Save_vip(id){
$.post("/update_vip/",{id:id,name:$("#"+id+"_name_vip_input").val(),mobile_phone:$("#"+id+"_mobile_phone_vip_input").val(),card_number:$("#"+id+"_card_number_vip_input").val()},function(data){
         if(data.sucess){
            $("#"+id+"_name_vip").html($("#"+id+"_name_vip_input").val());
            $("#"+id+"_mobile_phone_vip").html($("#"+id+"_mobile_phone_vip_input").val());
             $("#"+id+"_card_number_vip").html($("#"+id+"_card_number_vip_input").val());
            $("#"+id+"_save_vip").html("<a onclick='return Delete_vip("+id+");'>删除</a>");
            return false;
         }
         else
           alert("操作失败！");
           return false
        });
}

//删除会员信息
function Delete_vip(id){
 if(confirm("确认删除该记录？")){
             $.post("/delete_vip/",{id:id},function(data){
             if(data.sucess){
                 $("#"+id+"_tr_vip").remove();
                 return false;
             }
             else
               alert("操作失败！");
               return false
            });
    }else{
        return false;
    }
}
